fun main() {
    println("Hello, world!!!")
}

fun varDeclarations() {
    // declare your name and favorite hobby as constants, and your name as a regular variable.
    // print them using string interpolation in the format of:
    // "hi, my name is Charlie. I am 28 years old and my favorite hobby is pizza."
}

fun defaultValues(name: String, age: Number) {
    // Set a default value for name and then call the function with age only (as a named parameter) .
    println("$name is $age and likes to party")
}

// set "double" equal to a Lambda expression that takes in a number and returns it doubled.
val double

fun checkIfEligibleForIRA(oneYearOfExperience: Boolean) {
    // create a constant variable called "elligible".
    // If "oneYearOfExperience" is true, set it to "you are elligable".
    // otherwise, set it equal to "you are not elligable".
    // print "elligable".
}


fun loopUntilMaxNumber(maxNumber: Number = 10) {
	// loop from one until maxNumber and print the iterator value each time.
	// try it with both the default argument and passing in your own.    
}

fun hobbiesList() {
    // create a mutable list of your favorite hobbies and loop over it, printing each one.
    // at the end of the loop, print the size of your hobby list.
}

fun dogMap() {
    // create a map where of dog names to breeds.
    // print out one of the dog's breeds by reading it from your map.
}

// fill in the class person with the parameters of name, age, and favorite food.
class Person() {
    // in this method, print the person's name, age, and favorite food.
    fun descPerson() {
        
    }
}

fun makePerson() {
    // instantiate your person class here and call "descPerson"
}

// make a favoriteMovie data class here that contains name, genre, and release year.

// instantiaite your favorite movie and store it in a constant variable.

