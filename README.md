# Kotlin, Kroger, and You
### What is Kotlin?
- Kotlin is a very robust, staticly typed, language developed by JetBrains, the makers of IntelliJ
- It is `interoperable` with Java, meaning that you can add Kotlin code to an existing Java codebase, or do a whole application in only Kotlin and have it run on the JVM.
- Kotlin has many uses, but is primarily used when working on Native Android Applications and back-end web services.

### Setup
- First, install Open JDK through the Kroger Self Service application
- Next, you need to install IntelliJ IDEA. I recommend installing the Toolbox app, and installing through there (https://www.jetbrains.com/toolbox-app/)
    - Once you have installed, you need to open it and click License Server. Enter `http://intellij.kroger.com/licenseServer`, and click Activate.
- After that, click New Project > check Kotlin/JVM > Next > Finish
    - This should proceed to install any missing dependencies.